# a8z.lu - Coming Soon Page

**This is the coming soon / announcement page for [**a8z**](https://a8z.lu).**

With little of imagination, this will be a super tiny landing page.

### License
[MIT](https://opensource.org/licenses/MIT)

Copyright (c) 2020 Andy Frantz